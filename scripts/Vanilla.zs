//Vanilla Crafting Items

import mods.jei.JEI;


//1 Clay Block to 4 Clay Balls
recipes.addShapeless("Clay",<minecraft:clay_ball> * 4,[<minecraft:clay>]);

//1 Snow Block to 4 Snow Balls
recipes.addShapeless("Snow",<minecraft:snowball> * 4,[<minecraft:snow>]);

//2 Logs to 16 Sticks
recipes.addShapeless("Sticks",<minecraft:stick> * 16, [<ore:logWood>,<ore:logWood>]);

//3 Gravel to 1 Flint
recipes.addShapeless("Flint",<minecraft:flint>, [<minecraft:gravel>,<minecraft:gravel>,<minecraft:gravel>]);

//8 Logs to Chests
recipes.addShaped(<minecraft:chest> * 4,
 [[<ore:logWood>, <ore:logWood>, <ore:logWood>],
  [<ore:logWood>, null, <ore:logWood>],
  [<ore:logWood>, <ore:logWood>, <ore:logWood>]]);
  
//8 Saddle
recipes.addShaped(<minecraft:saddle>,
 [[<minecraft:leather>, <minecraft:leather>, <minecraft:leather>],
  [<minecraft:string>, null, <minecraft:string>],
  [<minecraft:iron_ingot>, null, <minecraft:iron_ingot>]]);