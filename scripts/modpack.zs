//Vanilla Crafting Items

import mods.jei.JEI;

//#Remove
recipes.removeShapeless(<sync:item_placeholder>, [<minecraft:daylight_detector>, <minecraft:lapis_block>, <minecraft:daylight_detector>,<minecraft:quartz>, <minecraft:beacon>, <minecraft:quartz>,<minecraft:emerald>, <minecraft:redstone_block>, <minecraft:emerald>]);
recipes.removeShaped(<sync:item_placeholder>, [[<minecraft:daylight_detector>, <minecraft:lapis_block>, <minecraft:daylight_detector>],[<minecraft:quartz>, <minecraft:beacon>, <minecraft:quartz>],[<minecraft:emerald>, <minecraft:redstone_block>, <minecraft:emerald>]]);
recipes.removeShaped(<sync:item_placeholder>, [[<minecraft:daylight_detector>, <minecraft:lapis_block>, <minecraft:daylight_detector>],[<minecraft:quartz>, <minecraft:ender_pearl>, <minecraft:quartz>],[<minecraft:emerald>, <minecraft:redstone_block>, <minecraft:emerald>]]);

//#Add
recipes.addShaped(<immersiverailroading:item_rail_part> * 4,[[<thermalfoundation:material:160>, <thermalfoundation:material:160>, <thermalfoundation:material:160>],[null, <thermalfoundation:material:160>, null],[<thermalfoundation:material:160>, <thermalfoundation:material:160>, <thermalfoundation:material:160>]]);
recipes.addShapeless(<sync:item_placeholder>, [<minecraft:daylight_detector>,<minecraft:lapis_block>,<minecraft:daylight_detector>,<minecraft:quartz>,<minecraft:diamond>,<minecraft:quartz>,<minecraft:emerald>,<minecraft:redstone_block>,<minecraft:emerald>]);
recipes.addShapeless(<minecraft:glowstone_dust> * 9,[<ore:glowstone>]);
recipes.addShapeless(<immersiveengineering:metal:38>, [<ore:plateSteel>]);
recipes.addShapeless(<minecraft:nether_star>,[<extrautils2:compresseddirt:3>,<extrautils2:compressedcobblestone:7>,<extrautils2:compresseddirt:3>,<extrautils2:compressedcobblestone:7>,<extrautils2:compressedcobblestone:7>,<extrautils2:compressedcobblestone:7>,<extrautils2:compresseddirt:3>,<extrautils2:compressedcobblestone:7>,<extrautils2:compresseddirt:3>]);
recipes.addShapeless(<chisel:glowstone1:2> * 9,[<minecraft:glowstone>,<minecraft:glowstone>,<minecraft:glowstone>,<minecraft:glowstone>,<minecraft:glowstone>,<minecraft:glowstone>,<minecraft:glowstone>,<minecraft:glowstone>,<minecraft:glowstone>]);
recipes.addShaped(<minecraft:bedrock>, [[<enderio:block_infinity:2>, <enderio:block_infinity:2>, <enderio:block_infinity:2>],[<enderio:block_infinity:2>, <enderio:block_infinity:2>, <enderio:block_infinity:2>], [<enderio:block_infinity:2>, <enderio:block_infinity:2>, <enderio:block_infinity:2>]]);
recipes.addShaped(<enderio:item_material:20>, [[<extrautils2:compressedcobblestone:7>, <extrautils2:compressedcobblestone:7>, <extrautils2:compressedcobblestone:7>],[<extrautils2:compressedcobblestone:7>, <extrautils2:compressedcobblestone:7>, <extrautils2:compressedcobblestone:7>], [<extrautils2:compressedcobblestone:7>, <extrautils2:compressedcobblestone:7>, <extrautils2:compressedcobblestone:7>]]);
recipes.addShapeless(<bigreactors:reactorcreativecoolantport>, [<bigreactors:reactorcasing>,<minecraft:hopper>,<bigreactors:reactorcasing>,<minecraft:diamond>,<extrautils2:drum:4>.withTag({Fluid: {FluidName: "water", Amount: 1}}).onlyWithTag({Fluid: {FluidName: "water", Amount: 1}}),<minecraft:diamond>,<bigreactors:reactorcasing>,<minecraft:piston>,<bigreactors:reactorcasing>]);
recipes.addShapeless(<extrautils2:drum:4>.withTag({Fluid: {FluidName: "oil", Amount: 1}}), [<extrautils2:drum:4>,<forge:bucketfilled>]);
recipes.addShapeless(<extrautils2:drum:4>.withTag({Fluid: {FluidName: "lava", Amount: 1}}), [<extrautils2:drum:4>,<minecraft:lava_bucket>]);
recipes.addShapeless(<extrautils2:drum:4>.withTag({Fluid: {FluidName: "water", Amount: 1}}), [<extrautils2:drum:4>,<minecraft:water_bucket>]);
recipes.addShaped(<extrautils2:passivegenerator:6>, [[<minecraft:bedrock>, <minecraft:ender_eye>, <minecraft:bedrock>],[<minecraft:nether_star>, <extrautils2:ingredients:1>, <minecraft:nether_star>], [<minecraft:bedrock>, <enderio:item_material:20>, <minecraft:bedrock>]]);
recipes.addShaped(<extrautils2:drum:4>, [[<minecraft:bedrock>, <enderio:item_material:20>, <minecraft:bedrock>],[<minecraft:bedrock>, <extrautils2:drum:3>, <minecraft:bedrock>], [<minecraft:bedrock>, <enderio:item_material:20>, <minecraft:bedrock>]]);
recipes.addShaped(<extrautils2:ingredients:11>, [[<extrautils2:compressednetherrack:5>, <extrautils2:compressednetherrack:5>, <extrautils2:compressednetherrack:5>],[<extrautils2:compressednetherrack:5>, <extrautils2:compressednetherrack:5>, <extrautils2:compressednetherrack:5>], [<extrautils2:compressednetherrack:5>, <extrautils2:compressednetherrack:5>, <extrautils2:compressednetherrack:5>]]);
recipes.addShaped(<extrautils2:itemcreativebuilderswand>, [[null, null, <enderio:item_material:20>],[null, <extrautils2:compressedcobblestone:7>, null], [<extrautils2:compressedcobblestone:7>, null, null]]);
recipes.addShaped(<extrautils2:itemcreativedestructionwand>, [[null, <enderio:item_material:20>, <enderio:item_material:20>],[null, <extrautils2:compressedcobblestone:7>, <enderio:item_material:20>], [<extrautils2:compressedcobblestone:7>, null, null]]);
recipes.addShaped(<thermalfoundation:upgrade:256>, [[<thermalfoundation:upgrade:35>, <thermalfoundation:upgrade:35>, <thermalfoundation:upgrade:35>],[<thermalfoundation:upgrade:35>, <thermalfoundation:upgrade:35>, <thermalfoundation:upgrade:35>], [<thermalfoundation:upgrade:35>, <thermalfoundation:upgrade:35>, <thermalfoundation:upgrade:35>]]);
