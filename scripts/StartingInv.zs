//Starting Inventory

mods.initialinventory.InvHandler.addStartingItem(<minecraft:torch> * 64);
mods.initialinventory.InvHandler.addStartingItem(<harvestcraft:pbandjitem> *8);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:diamond_sword>);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:diamond_pickaxe>);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:diamond_axe>);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:diamond_shovel>);
